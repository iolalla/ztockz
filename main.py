# pip install flet prophet yfinance plotly kaleido matplotlib
import flet as ft
from flet.plotly_chart import PlotlyChart
from flet.matplotlib_chart import MatplotlibChart
import matplotlib
import matplotlib.pyplot as plt
from datetime import date
import logging
logging.getLogger("flet_core").setLevel(logging.INFO)
import yfinance as yf
from prophet import Prophet
from prophet.plot import plot_plotly
import plotly.graph_objects as go
import plotly.offline as py
import warnings
warnings.simplefilter("ignore", UserWarning)
warnings.simplefilter("ignore", FutureWarning)

# We need this variable as global to connect the slider and the get_data
n_years = 1

# Plot raw data
def plot_raw_data(data, page: ft.Page):
	# fig = go.Figure()
	# fig.add_trace(go.Scatter(x=data['Date'], y=data['Open'], name="stock_open"))
	# fig.add_trace(go.Scatter(x=data['Date'], y=data['Close'], name="stock_close"))
    fig = go.Figure(data=go.Ohlc(x=data['Date'],
                open=data['Open'],
                high=data['High'],
                low=data['Low'],
                close=data['Close']))
    fig.layout.update(xaxis_rangeslider_visible=True)
    # fig = px.line(data, x=data['Date'], y=data['Open'] name="stock_open"),
    page.add(PlotlyChart(fig, isolated=True))
# Load data from Yahoo!
def load_data(ticker, years):
    # print(ticker)
    # print(f"Years %s", str(years))
    year = 2022 - years
    START = str(int(year)) + "-01-01"
    # print(f"Years %s", str(START))
    TODAY = date.today().strftime("%Y-%m-%d")
    data = yf.download(ticker, START, TODAY)
    data.reset_index(inplace=True)
    return data
# This function is to reset the controls 
def reset(page: ft.Page):
    # print(len(page._controls))
    i = 0
    if len(page._controls) > 6:
        while i < 9:
            page.controls.pop()
            i += 1
    page.update()
# This function defines the basics to show the data
def show_data(data, page: ft.Page):
            subheader  = ft.Text('Raw data')
            datatail = ft.Text(data.tail())
            page.add(subheader,datatail)
            plot_raw_data(data, page)
            page.update()
            # Predict forecast with Prophet.
            df_train = data[['Date','Close']]
            df_train = df_train.rename(columns={"Date": "ds", "Close": "y"})
            # Use Prophet as the basic tool to predict the time series
            m = Prophet()
            m.fit(df_train)
            period = 365
            future = m.make_future_dataframe(periods=period)
            forecast = m.predict(future)
            # Show and plot forecast
            predict_subheader = ft.Text('Forecast data')
            predict_forecast = ft.Text(forecast.tail())
            page.add(predict_subheader, predict_forecast)
            plot_title = ft.Text(f'Forecast plot prediction for 1 year')
            page.add(plot_title)
            fig1 = plot_plotly(m, forecast)
            fig1.update_layout(clickmode='event+select')
            page.add(PlotlyChart(fig1))
            forecast_title = ft.Text("Forecast components")
            page.add(forecast_title)
            fig2 = m.plot_components(forecast)
            page.add(MatplotlibChart(fig2))
# Main method where the magic happens
def main(page: ft.Page):
        def button_clicked(e):
            data_load_state.value = f"Dropdown value is:  {dd.value}"
            data = load_data(dd.value, n_years)
            data_load_state.value = f'Loading data...{dd.value} done!'
            reset(page)
            show_data(data, page)
            # logging.INFO("%s", str({dd.value}))

        def slider_changed(e):
            t.value = f"{e.control.value} years to train"
            global n_years 
            n_years = e.control.value
            page.update()

        page.title = "Stock Forecast App"
        page.horizontal_alignment = ft.CrossAxisAlignment.CENTER
        page.scroll = ft.ScrollMode.ADAPTIVE

        b = ft.ElevatedButton(text="Submit", on_click=button_clicked)
        dd = ft.Dropdown(
            hint_text="Choose your Stock",
            width=100,
            options=[
                ft.dropdown.Option("GOOG"),
                ft.dropdown.Option("AAPL"),
                ft.dropdown.Option("MSFT"),
                ft.dropdown.Option("AMZN")
            ],
        )
        data_load_state = ft.Text('Loading data...')
        t = ft.Text()
        page.add(
                ft.Text("How many years do you want to use ?"),
                ft.Slider(min=0, max=10, divisions=5, label="{value} years", on_change=slider_changed), t)
        page.add(dd, b, data_load_state)

# flet run -w  main.py
ft.app(target=main, view=ft.WEB_BROWSER)