### Stocks Prediction App based in Flet and Prophet

I did this code to learn about [Flet](https://flet.dev/). 
I copied part of the code from [Patrick Loeber](https://github.com/patrickloeber/python-fun.git)

I am thinking about including the [code](https://www.kaggle.com/code/iolalla/ibex35-times-series-forecasting-comparison) in order to predict using tensorflow and make a more accurate prediction.

## How to use this?

Very straing forward, install [Flet](https://flet.dev/), I did it with the tar for Linux, pretty easy.

Then use your preferred virtual environment to install:

```
$ pip install flet prophet yfinance plotly kaleido
```

and then simply run the code: 

```
$ flet run -w  main.py
```

If you use this code I hope you enjoy it as I did.

## Screenshots 
![Screenshot1](img/screencapture-127-0-0-1-59263-2023-12-17-18_38_53.png)
![Screenshot2](img/screencapture-127-0-0-1-59263-2023-12-17-18_39_52.png)
![Screenshot3](img/screencapture-127-0-0-1-59263-2023-12-17-18_40_09.png)
